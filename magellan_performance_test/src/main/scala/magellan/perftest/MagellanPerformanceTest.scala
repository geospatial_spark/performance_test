package magellan.perftest

import org.apache.spark.sql.SparkSession
import magellan.{Point, Line, PolyLine, Polygon, BoundingBox}
import org.apache.spark.sql.types._
import org.apache.spark.storage._
import org.apache.spark.sql.magellan.dsl.expressions._


/**
  * A simple Scala application with an external dependency to
  * demonstrate building and submitting as well as creating an
  * assembly JAR.
  */
object MagellanPerformanceTest {
  def main(args: Array[String]) {
    val spark = SparkSession.builder.appName("magellanPerfTest").getOrCreate()
    import spark.implicits._
    val sc = spark.sparkContext

    val data_location = "/home/ec2-user/performance_testData/"
//    val data_location = "/home/hectormauer/Desktop/performance_testData/"

    def time[R](block: => R): Double = {
      val t0 = System.nanoTime()
      val result = block //call-by-name
      val t1 = System.nanoTime()
      val elapsed_time = (t1-t0)/1e6
      elapsed_time
    }

    println("time function loaded")

    val shapefileInputLocation = data_location + "nynta_17d"
    val nycZoningLocation = data_location + "nyz_zoning"
    val streetsLocation = data_location + "SimplifiedStreets_wgs84"
    val bigcsvInputLocation = data_location + "trips_noZeroes.csv"
    val splitcsvLocation = data_location + "trips_big_split2.csv"

    //loading times

//   val tripsSplit_time = {time(spark.read.format("com.databricks.spark.csv").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").option("magellan.index", "true").option("magellan.index.precision", "15").load(splitcsvLocation).withColumn("point", point($"pickup_longitude",$"pickup_latitude").as("pointsTrips")).persist(StorageLevel.MEMORY_AND_DISK))}
//   val bigTrips_time = {time(spark.read.format("com.databricks.spark.csv").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").option("magellan.index", "true").option("magellan.index.precision", "15").load(bigcsvInputLocation).withColumn("point", point($"pickup_longitude",$"pickup_latitude").as("pointsBigTrips")).persist(StorageLevel.MEMORY_AND_DISK))}
//   val neighborhoods_time = {time(spark.read.format("magellan").option("type","shapefile").option("magellan.index", "true").option("magellan.index.precision", "15").load(shapefileInputLocation).select($"polygon".as("neighborhoodsNYC"), $"metadata"("BoroName").as("neighborhood")).toDF().persist(StorageLevel.MEMORY_AND_DISK))}
//   val nyc_zoning_time = {time(spark.read.format("magellan").option("type","shapefile").option("magellan.index", "true").option("magellan.index.precision", "15").load(nycZoningLocation).select($"polygon".as("zoningNYC"), $"metadata"("ZONEDIST").as("ZONEDIST")).toDF().persist(StorageLevel.MEMORY_AND_DISK))}
//   val streets_time = {time(spark.read.format("magellan").option("type","shapefile").option("magellan.index", "true").option("magellan.index.precision", "15").load(streetsLocation).select($"polyline".as("street_geom"), $"metadata"("Label").as("StreetName")).toDF().persist(StorageLevel.MEMORY_AND_DISK))}
//
//   val loading_times = List(tripsSplit_time,bigTrips_time,neighborhoods_time,nyc_zoning_time,streets_time)

    val tripsIndex = spark.read.format("com.databricks.spark.csv").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").option("magellan.index", "true").option("magellan.index.precision", "15").load(splitcsvLocation).withColumn("point", point($"pickup_longitude",$"pickup_latitude").as("pointsTrips")).persist(StorageLevel.MEMORY_AND_DISK)
    val bigtripsIndex = spark.read.format("com.databricks.spark.csv").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").option("magellan.index", "true").option("magellan.index.precision", "15").load(bigcsvInputLocation).withColumn("point", point($"pickup_longitude",$"pickup_latitude").as("pointsBigTrips")).persist(StorageLevel.MEMORY_AND_DISK)
    val neighborhoodsWGS84 = spark.read.format("magellan").option("type","shapefile").option("magellan.index", "true").option("magellan.index.precision", "15").load(shapefileInputLocation).select($"polygon".as("neighborhoodsNYC"), $"metadata"("BoroName").as("neighborhood")).toDF().persist(StorageLevel.MEMORY_AND_DISK)
    val nycZoning = spark.read.format("magellan").option("type","shapefile").option("magellan.index", "true").option("magellan.index.precision", "15").load(nycZoningLocation).select($"polygon".as("zoningNYC"), $"metadata"("ZONEDIST").as("ZONEDIST")).toDF().persist(StorageLevel.MEMORY_AND_DISK)
    val streets = spark.read.format("magellan").option("type","shapefile").load(streetsLocation).select($"polyline".as("street_geom"), $"metadata"("Label").as("StreetName")).toDF().persist(StorageLevel.MEMORY_AND_DISK)

    tripsIndex.createOrReplaceTempView("tripsIndex")
    bigtripsIndex.createOrReplaceTempView("bigtripsIndex")
    neighborhoodsWGS84.createOrReplaceTempView("neighborhoods")
    nycZoning.createOrReplaceTempView("zones")
    streets.createOrReplaceTempView("streets")

    println("All data loaded!")
    // Attribute queries

    val attquery1 = "SELECT COUNT(*) FROM tripsIndex WHERE passenger_count > 2"
    val attquery2 = "SELECT COUNT(*) FROM tripsIndex WHERE trip_distance > 4.5"
    val attquery3 = "SELECT COUNT(*) FROM neighborhoods WHERE neighborhood LIKE '%Brooklyn%'"
    val attquery4 = "SELECT COUNT(*) FROM bigtripsIndex WHERE passenger_count > 2"
    val attquery5 = "SELECT COUNT(*) FROM bigtripsIndex WHERE trip_distance > 4.5"
    val attquery6 = "SELECT COUNT(*) FROM zones WHERE ZONEDIST LIKE '%PARK'"

    //Warm Up queries
    val wu_tripsSplit = {time(spark.sql(attquery1).collect())}
    val wu_bigTrips = {time(spark.sql(attquery4).collect())}
    val wu_neighborhoods = {time(spark.sql(attquery3).collect())}
//    val wu_nyc_zoning = {time(spark.sql(attquery6).collect())}
//    val wu_streets = {time(spark.sql("SELECT COUNT (*) FROM streets").collect())}

    val wu_times = List(wu_tripsSplit,wu_bigTrips,wu_neighborhoods)//,wu_nyc_zoning,wu_streets)

    //Containment queries (within)
    val queryWithin1 = tripsIndex.join(neighborhoodsWGS84).where($"point" within $"neighborhoodsNYC")
    val queryWithin2 = bigtripsIndex.join(neighborhoodsWGS84).where($"point" within $"neighborhoodsNYC")

    //Intersection queries
    val queryIntersects3 = neighborhoodsWGS84.join(nycZoning).where($"neighborhoodsNYC" intersects $"zoningNYC") //(result is 2721)
    val queryIntersects4 = streets.join(neighborhoodsWGS84).where($"street_geom" intersects $"neighborhoodsNYC")
    val queryIntersects5 = streets.join(nycZoning).where($"street_geom" intersects $"zoningNYC")
//
    println("queries Loaded!")

//    val attquery1Time = {time(spark.sql(attquery1).count())}
//    val attquery2Time = {time(spark.sql(attquery2).count())}
//    val attquery3Time = {time(spark.sql(attquery3).count())}
//    val attquery4Time = {time(spark.sql(attquery4).count())}
//    val attquery5Time = {time(spark.sql(attquery5).count())}
    val attquery6Time = {time(spark.sql(attquery6).count())}

//    val attquery4Count = spark.sql(attquery4).count()
//    val attquery5Count = spark.sql(attquery5).count()

//    val timesAttQueries = List(attquery1Time,attquery2Time,attquery3Time,attquery4Time,attquery5Time,attquery6Time)
    val timesAttQueries = List(attquery6Time)

    println("Attribute queries calculated!")

//    val withinquery1Time = {time(queryWithin1.count())}
//    val withinquery2Time = {time(queryWithin2.count())}
//
////    val withinquery2Count = queryWithin2.count()
//
//    val timesWithinQueries = List(withinquery1Time,withinquery2Time)
    println("Within queries calculated!")

   // val intersectsquery3Time = {time(queryIntersects3.count())}
   // val intersectsquery4Time = {time(queryIntersects4.count())}
   // val intersectsquery5Time = {time(queryIntersects5.count())}
   //
   // val timesIntersectsQueries = List(intersectsquery3Time,intersectsquery4Time,intersectsquery5Time)
   //
   //  println("Intersect Queries calculated!")

//    val listOfCounts = List(attquery4Count, attquery5Count)//,withinquery2Count)

    // val writeToFile = "A" ++ timesAttQueries ++  "W" ++ timesWithinQueries //++ "I" ++  timesIntersectsQueries//++ listOfCounts //

    // sc.parallelize(writeToFile).coalesce(1, true).saveAsTextFile("totalTimes" + System.nanoTime()/1e6)
    println("WU" ++ wu_times ++ "A" ++ timesAttQueries )//++  "W" ++ timesWithinQueries )//++ "I" ++  timesIntersectsQueries)//++ listOfCounts //)
    println("written to file!")
    spark.stop()
  }
}
// scalastyle:on println
