package geospark.perftest
import org.apache.spark.sql.SparkSession
import org.datasyslab.geospark.spatialRDD.PointRDD
import org.datasyslab.geospark.spatialRDD.PolygonRDD
import org.datasyslab.geospark.spatialRDD.CircleRDD
import org.datasyslab.geospark.spatialRDD.SpatialRDD
import org.datasyslab.geospark.enums.FileDataSplitter
import org.datasyslab.geospark.enums.IndexType
import org.datasyslab.geospark.enums.GridType
import org.apache.spark.storage.StorageLevel
import com.vividsolutions.jts.geom.Envelope
import com.vividsolutions.jts.geom.Polygon
import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.Point
import com.vividsolutions.jts.geom.GeometryFactory
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.datasyslab.geospark.spatialOperator.RangeQuery
import org.datasyslab.geospark.spatialOperator.JoinQuery
import org.datasyslab.geospark.spatialOperator.KNNQuery
import org.datasyslab.geospark.formatMapper.shapefileParser.ShapefileReader
import org.datasyslab.geosparksql.utils.{Adapter, GeoSparkSQLRegistrator}


//import org.apache.commons.csv.{CSVFormat, CSVPrinter}

/**
  * A simple Scala application with an external dependency to
  * demonstrate building and submitting as well as creating an
  * assembly JAR.
  */
object GeosparkPerformanceTest {
  def main(args: Array[String]) {
    val spark = SparkSession.builder.appName("GeoSparkPerformanceTest").getOrCreate()
    import spark.implicits._
    //val sc = new SparkContext(new SparkConf()) (Spark Session builder already creates a Sparkcontext. To retrieve it:
    val sc = spark.sparkContext

    // data path for the Amazon Linux Instance.
    val data_path = "/home/ec2-user/performance_testData/"
//    val data_path = "/home/hectormauer/Desktop/performance_testData/"

    GeoSparkSQLRegistrator.registerAll(spark.sqlContext)
    println("GeosparkSQLRegistrator Done!!")

    def time[R](block: => R): Double = {
      val t0 = System.nanoTime()
      val result = block //call-by-name
      val t1 = System.nanoTime()
      //println("Elapnsed time: " + (t1-t0))
      val elapsed_time = (t1-t0)/1e6
      elapsed_time
    }
    println("time function loaded")

    val shapefileInputLocation = data_path + "nynta_17d"
    var neighborhoodsRDD = new SpatialRDD[Geometry]
   val neighborhoods_time = {time(ShapefileReader.readToGeometryRDD(sc, shapefileInputLocation))}
    neighborhoodsRDD.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefileInputLocation)
//    val wu_neighborhoods = {time(neighborhoodsRDD.analyze())}
    neighborhoodsRDD.analyze()
    neighborhoodsRDD.spatialPartitioning(GridType.KDBTREE)
    neighborhoodsRDD.buildIndex(IndexType.QUADTREE, true)
    val neighborhoodsDF = Adapter.toDf(neighborhoodsRDD,spark).select($"rddshape".as("geometry"),$"_c2".as("neighborhood_name")).persist(StorageLevel.MEMORY_AND_DISK)
    neighborhoodsDF.createOrReplaceTempView("neighborhoods")
    val neighborhoodsDF_geom = spark.sql("SELECT ST_GeomFromWKT(geometry) as neighborhoods_geometry, 'neighborhoods_name'  FROM neighborhoods").persist(StorageLevel.MEMORY_AND_DISK)
    neighborhoodsDF_geom.createOrReplaceTempView("neighborhoods_geom")

    val input = data_path + "trips_noZeroes.csv"
    val bigTrips_time = {time(spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(input).persist(StorageLevel.MEMORY_AND_DISK))}
    val trips = spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(input).persist(StorageLevel.MEMORY_AND_DISK)
    trips.createOrReplaceTempView("trips")
    val tripsDf = spark.sql("select ST_Point(cast(trips.pickup_longitude as Decimal(24,20)), cast(trips.pickup_latitude as Decimal(24,20))) as pickup_points from trips").persist(StorageLevel.MEMORY_AND_DISK)
    tripsDf.createOrReplaceTempView("trips_geom")
    val tripsRDD = new SpatialRDD[Geometry]
    tripsRDD.rawSpatialRDD = Adapter.toRdd(tripsDf)
    // val wu_bigTrips = {time(tripsRDD.analyze())}
    tripsRDD.analyze()
    tripsRDD.spatialPartitioning(neighborhoodsRDD.getPartitioner)
    tripsRDD.buildIndex(IndexType.QUADTREE, true)

    //Splitted data set for the sake of thesting
    val inputSplit = data_path + "trips_big_split2.csv"
   val tripsSplit_time = {time(spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(inputSplit).persist(StorageLevel.MEMORY_AND_DISK))}
    val tripsSplit = spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(inputSplit).persist(StorageLevel.MEMORY_AND_DISK)
    tripsSplit.createOrReplaceTempView("tripsSplit")
    val tripsSplitDf = spark.sql("select ST_Point(cast(tripsSplit.pickup_longitude as Decimal (24,20)),cast(tripsSplit.pickup_latitude as Decimal(24,20))) as pickup_points from tripsSplit").persist(StorageLevel.MEMORY_AND_DISK)
    tripsSplitDf.createOrReplaceTempView("tripsSplit_geom")
    val tripsSplitRDD = new SpatialRDD[Geometry]
    tripsSplitRDD.rawSpatialRDD = Adapter.toRdd(tripsSplitDf)
//    val wu_tripsSplit = {time(tripsSplitRDD.analyze())}
    tripsSplitRDD.analyze()
    tripsSplitRDD.spatialPartitioning(neighborhoodsRDD.getPartitioner)
    tripsSplitRDD.buildIndex(IndexType.QUADTREE, true)

    val shapefilenyZoning = data_path + "nyz_zoning"
    var nyc_zoningDistrictsRDD = new SpatialRDD[Geometry]
    val nyc_zoning_time = {time(ShapefileReader.readToGeometryRDD(sc, shapefilenyZoning))}
    nyc_zoningDistrictsRDD.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefilenyZoning)
    // val wu_nyc_zoning = {time(nyc_zoningDistrictsRDD.analyze())}
    nyc_zoningDistrictsRDD.analyze()
    nyc_zoningDistrictsRDD.spatialPartitioning(neighborhoodsRDD.getPartitioner)
    nyc_zoningDistrictsRDD.buildIndex(IndexType.QUADTREE, true)
    val nyzoningDF = Adapter.toDf(nyc_zoningDistrictsRDD,spark).select($"rddshape".as("geometry"),$"_c2".as("zones")).persist(StorageLevel.MEMORY_AND_DISK)
    nyzoningDF.createOrReplaceTempView("nycZoningDistricts")
    val nyzoningDF_geom = spark.sql("SELECT ST_GeomFromWKT(geometry) as zones_geometry, zones  FROM nycZoningDistricts").persist(StorageLevel.MEMORY_AND_DISK)
    nyzoningDF_geom.createOrReplaceTempView("nyczoning_geom")

    val shapefileStreetsLocation = data_path + "SimplifiedStreets_wgs84"
    var streetsRDD = new SpatialRDD[Geometry]
   val streets_time = {time(ShapefileReader.readToGeometryRDD(sc, shapefileStreetsLocation))}
    streetsRDD.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefileStreetsLocation)
   // val wu_streets = {time(streetsRDD.analyze())}
    streetsRDD.analyze()
    streetsRDD.spatialPartitioning(neighborhoodsRDD.getPartitioner)
    streetsRDD.buildIndex(IndexType.QUADTREE, true)
    val streetsDF = Adapter.toDf(streetsRDD,spark).select($"rddshape".as("geometry")).persist(StorageLevel.MEMORY_AND_DISK)
    streetsDF.createOrReplaceTempView("streets")
    val streetsDF_geom = spark.sql("SELECT ST_GeomFromWKT(geometry) as streets_geometry FROM streets").persist(StorageLevel.MEMORY_AND_DISK)
    streetsDF_geom.createOrReplaceTempView("streets_geom")

    val geometryFactory=new GeometryFactory()
    val kNNQueryPoint=geometryFactory.createPoint(new Coordinate(-73.976, 40.751))

   val loading_times = List(tripsSplit_time,bigTrips_time, neighborhoods_time,nyc_zoning_time,streets_time)
//    val wu_times = List(wu_tripsSplit, wu_bigTrips,wu_neighborhoods,wu_nyc_zoning, wu_streets)

    println("All Data is Loaded")

    print("Loading queries")
    // 1. Attribute queries
    //tripsSplit is a subset of trips with > 65.000 records.
    val attquery1 = "SELECT COUNT(*) FROM tripsSplit WHERE passenger_count > 2"
    val attquery2 = "SELECT COUNT(*) FROM tripsSplit WHERE trip_distance > 4.5"
    // neighborhoods is a shapefile containing the 192 boroughs of NYC.
    val attquery3 = "SELECT COUNT(*) FROM neighborhoods WHERE neighborhood_name LIKE '%Brooklyn'"
    // trips is a big CSV containing > 12.000.000 records.
    val attquery4 = "SELECT COUNT(*) FROM trips WHERE passenger_count > 2"
    val attquery5 = "SELECT COUNT(*) FROM trips WHERE trip_distance > 4.5"
    val attquery6 = "SELECT COUNT(*) FROM nycZoningDistricts WHERE zones LIKE '%PARK'"

    //Warm Up queries
    val wu_tripsSplit = {time(spark.sql(attquery1).collect())}
    val wu_bigTrips = {time(spark.sql(attquery4).collect())}
    val wu_neighborhoods = {time(spark.sql(attquery3).collect())}
    val wu_nyc_zoning = {time(spark.sql(attquery6).collect())}
    val wu_streets = {time(spark.sql("SELECT COUNT (*) FROM streets").collect())}

    val wu_times = List(wu_tripsSplit,wu_bigTrips,wu_neighborhoods,wu_nyc_zoning,wu_streets)

    // 2. Within queries (points inside Polygons)
    val queryWithin1_flat = JoinQuery.SpatialJoinQuery(tripsSplitRDD, neighborhoodsRDD, true, false)
    // val queryWithin2 = JoinQuery.SpatialJoinQueryFlat(tripsRDD, neighborhoodsRDD, true, false)

    // 3. Intersect queries (polygon with polygon and maybe line with polygon???)
    val queryIntersects3 = "SELECT COUNT(*) FROM neighborhoods_geom, nyczoning_geom WHERE ST_INTERSECTS (neighborhoods_geometry,zones_geometry)"
    val queryIntersects4 = "SELECT COUNT(*) FROM neighborhoods_geom, streets_geom WHERE ST_INTERSECTS (streets_geometry, neighborhoods_geometry)"
    val queryIntersects5 = "SELECT COUNT(*) FROM nyczoning_geom, streets_geom WHERE ST_INTERSECTS (streets_geometry, zones_geometry)"

    var neighborhoodsRDD_planar = new SpatialRDD[Geometry]
    neighborhoodsRDD_planar.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefileInputLocation)
    neighborhoodsRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263")
    val neighborhoods_circle = new CircleRDD(neighborhoodsRDD_planar,100)

    var nyc_zoningDistrictsRDD_planar = new SpatialRDD[Geometry]
    nyc_zoningDistrictsRDD_planar.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefilenyZoning)
    nyc_zoningDistrictsRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263")
    val nyc_zoning_circle = new CircleRDD(nyc_zoningDistrictsRDD_planar,100)

    val tripsSplit_planar = spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(inputSplit).persist(StorageLevel.MEMORY_AND_DISK)
    tripsSplit_planar.createOrReplaceTempView("tripsSplit_planar")
    val tripsSplitDf_planar = spark.sql("select ST_Point(cast(pickup_longitude as Decimal (24,20)),cast(pickup_latitude as Decimal(24,20))) as pickup_points from tripsSplit_planar").persist(StorageLevel.MEMORY_AND_DISK)
    tripsSplitDf_planar.createOrReplaceTempView("tripsSplit_geom")
    val tripsSplitRDD_planar = new SpatialRDD[Geometry]
    tripsSplitRDD_planar.rawSpatialRDD = Adapter.toRdd(tripsSplitDf_planar)
    tripsSplitRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263")

    val trips_planar = spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(input).persist(StorageLevel.MEMORY_AND_DISK)
    trips_planar.createOrReplaceTempView("trips_planar")
    val trips_cleaned = spark.sql("SELECT * FROM trips_planar WHERE pickup_longitude > -80.0 AND pickup_longitude < -70.0 AND pickup_latitude > 35.0 AND pickup_latitude < 45.0")
    trips_cleaned.createOrReplaceTempView("trips_cleaned")
    val tripsPlanar_cleaned_geom = spark.sql("SELECT ST_Point(cast(pickup_longitude as Decimal (24,20)),cast(pickup_latitude as Decimal(24,20))) as pickup_points from trips_cleaned").persist(StorageLevel.MEMORY_AND_DISK)
    tripsPlanar_cleaned_geom.createOrReplaceTempView("trips_cleaned_geom")
    val tripsRDD_planar = new SpatialRDD[Geometry]
    tripsRDD_planar.rawSpatialRDD = Adapter.toRdd(tripsPlanar_cleaned_geom)
    tripsRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263")

    neighborhoods_circle.analyze()
    nyc_zoning_circle.analyze()
    tripsSplitRDD_planar.analyze()
    tripsRDD_planar.analyze()

    neighborhoods_circle.spatialPartitioning(GridType.KDBTREE)
    neighborhoods_circle.buildIndex(IndexType.QUADTREE, true)
    nyc_zoning_circle.spatialPartitioning(neighborhoods_circle.getPartitioner)
    nyc_zoning_circle.buildIndex(IndexType.QUADTREE, true)
    tripsSplitRDD_planar.spatialPartitioning(neighborhoods_circle.getPartitioner)
    tripsSplitRDD_planar.buildIndex(IndexType.QUADTREE, true)
    tripsRDD_planar.spatialPartitioning(neighborhoods_circle.getPartitioner)
    tripsRDD_planar.buildIndex(IndexType.QUADTREE, true)

    // 4. Distance query (buffer)
    val bufferQuery1 = JoinQuery.DistanceJoinQuery(tripsSplitRDD_planar, neighborhoods_circle, false, true)
    val bufferQuery2 = JoinQuery.DistanceJoinQuery(tripsSplitRDD_planar, nyc_zoning_circle, false, true)

    val bufferQuery3 = JoinQuery.DistanceJoinQuery(tripsRDD_planar, neighborhoods_circle, false, true)
    val bufferQuery4 = JoinQuery.DistanceJoinQuery(tripsRDD_planar, nyc_zoning_circle, false, true)

    println("Queries loaded")


    println("running and measuring time!")
    // AttributeQueries
    val attquery1Time = {time(spark.sql(attquery1).collect())}
    val attquery2Time = {time(spark.sql(attquery2).collect())}
    val attquery3Time = {time(spark.sql(attquery3).collect())}
    val attquery4Time = {time(spark.sql(attquery4).collect())}
    val attquery5Time = {time(spark.sql(attquery5).collect())}
    val attquery6Time = {time(spark.sql(attquery6).collect())}

    val timesAttQueries = List(attquery1Time,attquery2Time,attquery3Time,attquery4Time,attquery5Time,attquery6Time)

    //WithinQueries
    val queryWithin1Time_flat = {time(queryWithin1_flat.count())}
    // val queryWithin2Time = {time(queryWithin2.count())}

    val timesWithinQueries = List(queryWithin1Time_flat)//, queryWithin2Time)

    //IntersectQueries
    val intersectsquery3Time = {time(spark.sql(queryIntersects3).collect())}
    val intersectsquery4Time = {time(spark.sql(queryIntersects4).collect())}
    val intersectsquery5Time = {time(spark.sql(queryIntersects5).collect())}

    val timesIntersectsQueries = List(intersectsquery3Time,intersectsquery4Time,intersectsquery5Time)

    //BufferQuery
    val bufferQuery1Time = {time(bufferQuery1.count())}
    val bufferQuery2Time = {time(bufferQuery2.count())}
//    val bufferQuery3Time = {time(bufferQuery3.count())}
//    val bufferQuery4Time = {time(bufferQuery4.count())}
    val timesBufferQueries = List(bufferQuery1Time,bufferQuery2Time)//, bufferQuery3Time, bufferQuery4Time)

    //Knn Query
    val knnQuery1Time = {time(KNNQuery.SpatialKnnQuery(tripsSplitRDD, kNNQueryPoint, 1000,false))}
    val knnQuery2Time = {time(KNNQuery.SpatialKnnQuery(tripsRDD, kNNQueryPoint, 1000,false))}
    val timesknnQuery = List(knnQuery1Time,knnQuery2Time)



    // val writeToFile = "A" ++ timesAttQueries ++ "W" ++ timesWithinQueries ++ "I"++ timesIntersectsQueries  ++
      // "B" ++ timesBufferQueries ++ "K" ++ timesknnQuery

    // sc.parallelize(writeToFile).coalesce(1, true).saveAsTextFile("totalTimes" + System.nanoTime()/1e6)
    println("LD"++loading_times++"WU"++ wu_times++"A" ++ timesAttQueries ++ "W" ++ timesWithinQueries ++ "I" ++ timesIntersectsQueries ++ "B" ++ timesBufferQueries ++ "K" ++ timesknnQuery)
    spark.stop()
  }
}
// scalastyle:on println
