package geospark.bufferTransformation

import com.vividsolutions.jts.geom.Geometry
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.datasyslab.geospark.enums.{GridType, IndexType}
import org.datasyslab.geospark.formatMapper.shapefileParser.ShapefileReader
import org.datasyslab.geospark.spatialRDD.{CircleRDD, SpatialRDD}
import org.datasyslab.geosparksql.utils.{Adapter, GeoSparkSQLRegistrator}

object GeosparkBufferTransformation {
  def main(args: Array[String]) {
    val spark = SparkSession.builder.appName("GeoSparkPerformanceTest").getOrCreate()
//    import spark.implicits._
    //val sc = new SparkContext(new SparkConf()) (Spark Session builder already creates a Sparkcontext. To retrieve it:
    val sc = spark.sparkContext

    // data path for the Amazon Linux Instance.
    val data_path = "/home/ec2-user/performance_testData/"
//    val data_path = "/home/hectormauer/Desktop/performance_testData/"

    GeoSparkSQLRegistrator.registerAll(spark.sqlContext)
    println("GeosparkSQLRegistrator Done!!")

    def time[R](block: => R): Double = {
      val t0 = System.nanoTime()
      val result = block //call-by-name
      val t1 = System.nanoTime()
      //println("Elapnsed time: " + (t1-t0))
      val elapsed_time = (t1-t0)/1e6
      elapsed_time
    }
    println("time function loaded")
    val inputSplit = data_path + "trips_big_split2.csv"
    val tripsSplit_planar = spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(inputSplit).persist(StorageLevel.MEMORY_AND_DISK)
    tripsSplit_planar.createOrReplaceTempView("tripsSplit_planar")
    val tripsSplitDf_planar = spark.sql("select ST_Point(cast(pickup_longitude as Decimal (24,20)),cast(pickup_latitude as Decimal(24,20))) as pickup_points from tripsSplit_planar").persist(StorageLevel.MEMORY_AND_DISK)
    tripsSplitDf_planar.createOrReplaceTempView("tripsSplit_geom")

    val input = data_path + "trips_noZeroes.csv"
    val trips_planar = spark.read.format("csv").option("delimiter",",").option("mode", "DROPMALFORMED").option("inferSchema","true").option("header","true").load(input).persist(StorageLevel.MEMORY_AND_DISK)
    trips_planar.createOrReplaceTempView("trips_planar")
    val trips_cleaned = spark.sql("SELECT * FROM trips_planar WHERE pickup_longitude > -80.0 AND pickup_longitude < -70.0 AND pickup_latitude > 35.0 AND pickup_latitude < 45.0")
    trips_cleaned.createOrReplaceTempView("trips_cleaned")
    val tripsPlanar_cleaned_geom = spark.sql("SELECT ST_Point(cast(pickup_longitude as Decimal (24,20)),cast(pickup_latitude as Decimal(24,20))) as pickup_points from trips_cleaned").persist(StorageLevel.MEMORY_AND_DISK)
    tripsPlanar_cleaned_geom.createOrReplaceTempView("trips_cleaned_geom")

//  Transformation

   val shapefileInputLocation = data_path + "nynta_17d"
   var neighborhoodsRDD_planar = new SpatialRDD[Geometry]
   neighborhoodsRDD_planar.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefileInputLocation)
   val neigh_timeT1 = {time(neighborhoodsRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263"))}
   val neigh_timeT2 = {time(neighborhoodsRDD_planar.analyze())}
   val neigh_trans_time = neigh_timeT1 + neigh_timeT2


   val shapefilenyZoning = data_path + "nyz_zoning"
   var nyc_zoningDistrictsRDD_planar = new SpatialRDD[Geometry]
   nyc_zoningDistrictsRDD_planar.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefilenyZoning)
   val zones_timeT1 = {time(nyc_zoningDistrictsRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263"))}
   val zones_timeT2 = {time(nyc_zoningDistrictsRDD_planar.analyze())}
   val zones_trans_time = zones_timeT1 +zones_timeT2


   val tripsSplitRDD_planar = new SpatialRDD[Geometry]
   tripsSplitRDD_planar.rawSpatialRDD = Adapter.toRdd(tripsSplitDf_planar)
   val tripsSplit_timeT1 = {time(tripsSplitRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263"))}
   val tripsSplit_timeT2 = {time(tripsSplitRDD_planar.analyze())}
   val tripsSplit_trans_time = tripsSplit_timeT1 +tripsSplit_timeT2

   val tripsRDD_planar = new SpatialRDD[Geometry]
   tripsRDD_planar.rawSpatialRDD = Adapter.toRdd(tripsPlanar_cleaned_geom)
   val trips_timeT1 = {time(tripsRDD_planar.CRSTransform("EPSG:4326", "EPSG:2263"))}
   val trips_timeT2 = {time(tripsRDD_planar.analyze())}
   val trips_trans_time = trips_timeT1 + trips_timeT2

   val shapefileStreetsLocation = data_path + "SimplifiedStreets_wgs84"
   var streetsRDD = new SpatialRDD[Geometry]
   streetsRDD.rawSpatialRDD = ShapefileReader.readToGeometryRDD(sc, shapefileStreetsLocation)
   val streets_timeT1 = {time(streetsRDD.CRSTransform("EPSG:4326", "EPSG:2263"))}
   val streets_timeT2 = {time(streetsRDD.analyze())}
   val streets_trans_time = streets_timeT1 + streets_timeT2

   val trans_Times = List(tripsSplit_trans_time, trips_trans_time, neigh_trans_time, zones_trans_time, streets_trans_time)

   //Buffers


   val neighborhoods_circle = new CircleRDD(neighborhoodsRDD_planar,100)
   val neigh_buffer_time1 = {time(neighborhoods_circle.analyze())}
   val neigh_buffer_time2 = {time(neighborhoods_circle.spatialPartitioning(GridType.KDBTREE))}
   val neigh_buffer_time3 = {time(neighborhoods_circle.buildIndex(IndexType.QUADTREE, true))}
   val neighBufferTimes = neigh_buffer_time1 +neigh_buffer_time2 +neigh_buffer_time3

   val tripsSplit_circle = new CircleRDD(tripsSplitRDD_planar, 100)
   val tripsSplit_buffer_time1 = {time(tripsSplit_circle.analyze())}
   val tripsSplit_buffer_time2 = {time(tripsSplit_circle.spatialPartitioning(neighborhoods_circle.getPartitioner))}
   val tripsSplit_buffer_time3 = {time(tripsSplit_circle.buildIndex(IndexType.QUADTREE, true))}
   val tripsSplitBufferTimes = tripsSplit_buffer_time1 + tripsSplit_buffer_time2 + tripsSplit_buffer_time3

   val trips_circle = new CircleRDD(tripsRDD_planar, 100)
   val trips_buffer_time1 = {time(trips_circle.analyze())}
   val trips_buffer_time2 = {time(trips_circle.spatialPartitioning(neighborhoods_circle.getPartitioner))}
   val trips_buffer_time3 = {time(trips_circle.buildIndex(IndexType.QUADTREE, true))}
   val tripsBufferTimes = trips_buffer_time1 + trips_buffer_time2 + trips_buffer_time3

   val nyc_zoning_circle = new CircleRDD(nyc_zoningDistrictsRDD_planar,100)
   val zones_buffer_time1 = {time(nyc_zoning_circle.analyze())}
   val zones_buffer_time2 = {time(nyc_zoning_circle.spatialPartitioning(neighborhoods_circle.getPartitioner))}
   val zones_buffer_time3 = {time(nyc_zoning_circle.buildIndex(IndexType.QUADTREE, true))}
   val zonesBufferTimes = zones_buffer_time1 + zones_buffer_time2 + zones_buffer_time3

   val streets_circle = new CircleRDD(streetsRDD,100)
   val streets_buffer_time1 = {time(streets_circle.analyze())}
   val streets_buffer_time2 = {time(streets_circle.spatialPartitioning(neighborhoods_circle.getPartitioner))}
   val streets_buffer_time3 = {time(streets_circle.buildIndex(IndexType.QUADTREE, true))}
   val streetsBufferTimes = streets_buffer_time1 + streets_buffer_time2 + streets_buffer_time3

   val bufferTimes = List(tripsSplitBufferTimes,tripsBufferTimes,neighBufferTimes,zonesBufferTimes, streetsBufferTimes)


    println("TT"++trans_Times++"BT"++ bufferTimes)
    spark.stop()
  }
}
